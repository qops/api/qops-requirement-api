########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, backref

from qops_desktop import db


class Base(db.Model):
    __abstract__ = True
    __bind_key__ = 'qops'

    id = db.Column(db.Integer, primary_key = True)
    created_on = db.Column(db.DateTime, default=db.func.now())
    updated_on = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())


class Requirement(Base):
    __tablename__ = 'requirement'
    
    identifier = db.Column(db.String)
    name = db.Column(db.String)
    description = db.Column(db.String)
    kind_id = db.Column(db.Integer, ForeignKey('requirement_kind.id'))
    priority_id = db.Column(db.Integer, ForeignKey('requirement_priority.id'))
    state_id = db.Column(db.Integer, ForeignKey('requirement_state.id'))
    author_id = db.Column(db.Integer, ForeignKey('person.id'))
    owner_id = db.Column(db.Integer, ForeignKey('person.id'))

    def get_next_identifier():
        return 'REQ00001'


class RequirementChampions(Base):
    __tablename__ = 'requirement_champions'
    
    requirement_id = db.Column(db.Integer, ForeignKey('requirement.id'))
    person_id = db.Column(db.Integer, ForeignKey('person.id'))


class RequirementStateHistory(Base):
    __tablename__ = 'requirement_state_history'
    
    requirement_id = db.Column(db.Integer)
    original_state_id = db.Column(db.Integer)
    new_state_id = db.Column(db.Integer)
    person_id = db.Column(db.Integer)


class RequirementKind(Base):
    __tablename__ = 'requirement_kind'
    
    identifier = db.Column(db.String)
    name = db.Column(db.String)
    usage = db.Column(db.String)


class RequirementPriority(Base):
    __tablename__ = 'requirement_priority'
    
    identifier = db.Column(db.String)
    name = db.Column(db.String)
    usage = db.Column(db.String)


class RequirementState(Base):
    __tablename__ = 'requirement_state'
    
    identifier = db.Column(db.String)
    name = db.Column(db.String)
    usage = db.Column(db.String)
