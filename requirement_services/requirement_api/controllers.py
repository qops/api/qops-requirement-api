########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


from flask import Blueprint, request, render_template, redirect, url_for

from .models import Requirement
from .models import RequirementKind
from .models import RequirementPriority
from .models import RequirementState
from ..mod_person.models import Person

from .forms import RequirementProfileForm

from qops_desktop import db


mod_requirement = Blueprint('requirement', __name__, url_prefix='/requirement')


@mod_requirement.context_processor
def store():
    store_dict = {'serviceName': 'Requirement',
                  'serviceDashboardUrl': url_for('requirement.dashboard'),
                  'serviceBrowseUrl': url_for('requirement.browse'),
                  'serviceNewUrl': url_for('requirement.new'),
                  }
    return store_dict


@mod_requirement.route('/', methods=['GET'])
def requirement():
    return render_template('requirement/requirement_dashboard.html')


@mod_requirement.route('/dashboard', methods=['GET'])
def dashboard():
    return render_template('requirement/requirement_dashboard.html')


@mod_requirement.route('/browse', methods=['GET'])
def browse():
    requirements = Requirement.query.with_entities(Requirement.id, Requirement.name, RequirementKind.name.label('kind_id'), RequirementPriority.name.label('priority_id'), RequirementState.name.label('state_id')).join(
        RequirementPriority, Requirement.priority_id == RequirementPriority.id).join(RequirementKind, Requirement.kind_id == RequirementKind.id).join(RequirementState, Requirement.state_id == RequirementState.id).all()
    return render_template('requirement/requirement_browse.html', requirements=requirements)


@mod_requirement.route('/new', methods=['GET', 'POST'])
def new():
    requirement = Requirement()
    form = RequirementProfileForm(request.form)
    form.kind_id.choices = [(k.id, k.name)
                            for k in RequirementKind.query.all()]
    form.priority_id.choices = [(p.id, p.name)
                                for p in RequirementPriority.query.all()]
    form.state_id.choices = [(s.id, s.name)
                             for s in RequirementState.query.all()]
    form.author_id.choices = [(a.id, a.first_name) for a in Person.query.all()]
    form.owner_id.choices = [(o.id, o.first_name) for o in Person.query.all()]

    if request.method == 'POST':
        form.populate_obj(requirement)
        requirement.identifier = Requirement.get_next_identifier()
        db.session.add(requirement)
        db.session.commit()
        return redirect(url_for('requirement.browse'))
    return render_template('requirement/requirement_new.html', requirement=requirement, form=form)


@mod_requirement.route('/profile', methods=['GET', 'POST'])
@mod_requirement.route('/profile/<int:requirement_id>/profile', methods=['GET', 'POST'])
def profile(requirement_id=None):
    requirement = Requirement.query.get(requirement_id)
    form = RequirementProfileForm(obj=requirement)
    form.kind_id.choices = [(k.id, k.name)
                            for k in RequirementKind.query.all()]
    form.priority_id.choices = [(p.id, p.name)
                                for p in RequirementPriority.query.all()]
    form.state_id.choices = [(s.id, s.name)
                             for s in RequirementState.query.all()]
    form.author_id.choices = [(a.id, a.first_name) for a in Person.query.all()]
    form.owner_id.choices = [(o.id, o.first_name) for o in Person.query.all()]
    if request.method == 'POST':
        form = RequirementProfileForm(request.form)
        form.populate_obj(requirement)
        db.session.add(requirement)
        db.session.commit()
        return redirect(url_for('requirement.browse'))
    return render_template('requirement/requirement_profile.html', requirement=requirement, form=form)


@mod_requirement.route('/view', methods=['GET', 'POST'])
@mod_requirement.route('/view/<int:requirement_id>/view', methods=[
    'GET', 'POST'])
def requirement_view(requirement_id=None):
    #requirement = Requirement.query.get(requirement_id)
    form = RequirementProfileForm(obj=requirement)
    form.kind_id.choices = [(k.id, k.name)
                            for k in RequirementKind.query.all()]
    form.priority_id.choices = [(p.id, p.name)
                                for p in RequirementPriority.query.all()]
    form.state_id.choices = [(s.id, s.name)
                             for s in RequirementState.query.all()]
    form.author_id.choices = [(a.id, a.first_name) for a in Person.query.all()]
    form.owner_id.choices = [(o.id, o.first_name) for o in Person.query.all()]
    if request.method == 'POST':
        form = RequirementProfileForm(request.form)
        form.populate_obj(requirement)
        db.session.add(requirement)
        db.session.commit()
        return redirect(url_for('requirement.browse'))
    return render_template('requirement/requirement_view.html',
                           requirement=requirement, form=form)


@mod_requirement.route('/profile/dashboard', methods=['GET'])
@mod_requirement.route('/profile/<int:requirement_id>/dashboard', methods=['GET'])
def requirement_dashboard(requirement_id=None):
    if requirement_id:
        requirement = Requirement.query.get(requirement_id)
    else:
        requirement = None
    return render_template('requirement/requirement_single_dashboard.html', requirement=requirement)


@mod_requirement.route('/profile/tasks', methods=['GET'])
@mod_requirement.route('/profile/<int:requirement_id>/tasks', methods=['GET'])
def requirement_tasks(requirement_id=None):
    if requirement_id:
        requirement = Requirement.query.get(requirement_id)
    else:
        requirement = None
    return render_template('requirement/requirement_single_tasks.html', requirement=requirement)


@mod_requirement.route('/profile/communication', methods=['GET', 'POST'])
@mod_requirement.route('/profile/<int:requirement_id>/communication', methods=['GET'])
def requirement_communication(requirement_id=None):
    if requirement_id:
        requirement = Requirement.query.get(requirement_id)
    else:
        requirement = None
    return render_template('requirement/requirement_single_communication.html', requirement=requirement)


@mod_requirement.route('/profile/documents', methods=['GET', 'POST'])
@mod_requirement.route('/profile/<int:requirement_id>/documents', methods=['GET'])
def requirement_documents(requirement_id=None):
    if requirement_id:
        requirement = Requirement.query.get(requirement_id)
    else:
        requirement = None
    return render_template('requirement/requirement_single_documents.html', requirement=requirement)


@mod_requirement.route('/profile/builds', methods=['GET', 'POST'])
@mod_requirement.route('/profile/<int:requirement_id>/builds', methods=['GET'])
def requirement_builds(requirement_id=None):
    if requirement_id:
        requirement = Requirement.query.get(requirement_id)
    else:
        requirement = None
    return render_template('requirement/requirement_single_builds.html', requirement=requirement)


@mod_requirement.route('/profile/requirements', methods=['GET', 'POST'])
@mod_requirement.route('/profile/<int:requirement_id>/requirements', methods=['GET'])
def requirement_requirements(requirement_id=None):
    if requirement_id:
        requirement = Requirement.query.get(requirement_id)
    else:
        requirement = None
    return render_template('requirement/requirement_single_requirements.html', requirement=requirement)


@mod_requirement.route('/profile/customers', methods=['GET', 'POST'])
@mod_requirement.route('/profile/<int:requirement_id>/customers', methods=['GET'])
def requirement_customers(requirement_id=None):
    if requirement_id:
        requirement = Requirement.query.get(requirement_id)
    else:
        requirement = None
    return render_template('requirement/requirement_single_customers.html', requirement=requirement)


@mod_requirement.route('/profile/locations', methods=['GET', 'POST'])
@mod_requirement.route('/profile/<int:requirement_id>/builds', methods=['GET'])
def requirement_locations(requirement_id=None):
    if requirement_id:
        requirement = Requirement.query.get(requirement_id)
    else:
        requirement = None
    return render_template('requirement/requirement_single_locations.html', requirement=requirement)


@mod_requirement.route('/profile/issues', methods=['GET', 'POST'])
@mod_requirement.route('/profile/<int:requirement_id>/issues', methods=['GET'])
def requirement_issues(requirement_id=None):
    if requirement_id:
        requirement = Requirement.query.get(requirement_id)
    else:
        requirement = None
    return render_template('requirement/requirement_single_issues.html', requirement=requirement)


@mod_requirement.route('/profile/test-cases', methods=['GET', 'POST'])
@mod_requirement.route('/profile/<int:requirement_id>/test-cases', methods=['GET'])
def requirement_test_cases(requirement_id=None):
    if requirement_id:
        requirement = Requirement.query.get(requirement_id)
    else:
        requirement = None
    return render_template('requirement/requirement_single_test_cases.html', requirement=requirement)
